﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4___Ivan_Kikic
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();

    }
}
