﻿using System;
using System.Collections.Generic;

namespace LV4___Ivan_Kikic
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset file = new Dataset("ivankikic.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);

            double[] print2 = adapter.CalculateAveragePerRow(file);

            foreach (double i in print2)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("\n");
            double[] print = adapter.CalculateAveragePerColumn(file);

            foreach (double i in print)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("\n");
            List<IRentable> List = new List<IRentable>();
            Book book = new Book("Programming");
            Video video = new Video("The Shawshank Redemption");
            List.Add(book);
            List.Add(video);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.PrintTotalPrice(List);
            printer.DisplayItems(List);
            Console.WriteLine("\n");

            HotItem HitKnjiga = new HotItem(new Book("Hit Knjiga"));
            HotItem HitFilm = new HotItem(new Video("Hit Film"));
            List.Add(HitKnjiga);
            List.Add(HitFilm);
            printer.PrintTotalPrice(List);
            printer.DisplayItems(List);
            Console.WriteLine("\n");
        }
    }
}
