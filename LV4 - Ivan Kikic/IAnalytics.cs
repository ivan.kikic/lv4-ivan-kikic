﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4___Ivan_Kikic
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);

    }
}
