﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4___Ivan_Kikic
{
    class Book : IRentable

    {
        public String Title { get; private set; }
        private readonly double BaseBookPrice = 3.99;

        public Book(String title) { this.Title = title; }

        public string Description { get { return this.Title; } }

        public double CalculatePrice()
        {
            return BaseBookPrice;
        }
    }
}
