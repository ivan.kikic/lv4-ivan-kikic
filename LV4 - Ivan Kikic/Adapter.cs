﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4___Ivan_Kikic
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> data = dataset.GetData();

            int rowCount = data.Count;
            int colCount = data[0].Count;

            double[][] matrix = new double[rowCount][];

            for (int i = 0; i < rowCount; i++)
            {
                matrix[i] = new double[colCount];
                for(int j = 0; j < colCount; j++)
                {
                    matrix[i][j] = data[i][j];
                }
            }
            return matrix;

        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
